<?php
/**
 * 企业微信SDK
 *
 * @copyright (c) 2023-2024 上海宙品信息科技有限公司
 */

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__ . "/src")
    ->name("*.php")
    ->notname("*.blade.php")
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

return (new PhpCsFixer\Config())
    ->setRules([

        '@PSR12' => true,

        /**
         * Array Notation
         */
        'array_indentation'                           => true,
        'array_syntax'                                => ['syntax' => 'short'],
        'no_whitespace_before_comma_in_array'         => true,
        'no_multiline_whitespace_around_double_arrow' => true,
        'trim_array_spaces'                           => true,
        'whitespace_after_comma_in_array'             => ['ensure_single_space' => true],
        'normalize_index_brace'                       => true,
        'no_trailing_comma_in_singleline'             => true,

        /**
         * Casing
         */
        'constant_case'          => ['case' => 'lower'],
        'native_function_casing' => true,

        /**
         * Cast Notation
         */
        'lowercase_cast'    => true,
        'short_scalar_cast' => true,
        // 'no_short_bool_cast' => true,

        /**
         * Class Notation
         */
        'class_attributes_separation' => [
            'elements' => ['method' => 'one'],
        ],
        'no_blank_lines_after_class_opening' => true,
        'single_class_element_per_statement' => true,
        // 'class_definition' => array('singleLine' => true),

        /**
         * Comment
         */
        'single_line_comment_style' => ['comment_types' => ['hash']],
        // 'no_empty_comment' => true,

        /**
         * Control Structure
         */
        'include'                     => true,
        'trailing_comma_in_multiline' => ['elements' => ['arrays']],
        // 'no_unneeded_control_parentheses' => true,

        /**
         * Function Notation
         */
        'type_declaration_spaces' => true,
        'return_type_declaration' => true,

        /**
         * Language Construct
         */
        'combine_consecutive_unsets' => true,
        'declare_equal_normalize'    => true,

        /**
         * Namespace Notation
         */
        'no_leading_namespace_whitespace' => true,

        /**
         * Operator
         */
        'binary_operator_spaces' => [
            'operators' => [
                '=>' => 'align_single_space_minimal',
                '='  => 'align_single_space_minimal',
            ],
        ],
        'concat_space'            => ['spacing' => 'one'],
        'ternary_operator_spaces' => true,
        'unary_operator_spaces'   => true,
        'standardize_not_equals'  => true,

        'object_operator_without_whitespace' => true,

        /**
         * PHPUnit
         */
        // 'php_unit_fqcn_annotation' => true,

        /**
         * PHPDoc
         */
        'phpdoc_align'            => true,
        'align_multiline_comment' => true,
        'no_empty_phpdoc'         => true,
        // 'phpdoc_annotation_without_dot' => true,
        // 'phpdoc_indent' => true,
        // 'phpdoc_inline_tag' => true,
        // 'phpdoc_no_access' => true,
        // 'phpdoc_no_alias_tag' => true,
        // 'phpdoc_no_empty_return' => true,
        // 'phpdoc_no_package' => true,
        // 'phpdoc_no_useless_inheritdoc' => true,
        // 'phpdoc_return_self_reference' => true,
        // 'phpdoc_scalar' => true,
        // 'phpdoc_separation' => true,
        // 'phpdoc_single_line_var_spacing' => true,
        // 'phpdoc_summary' => true,
        // 'phpdoc_to_comment' => true,
        // 'phpdoc_trim' => true,
        // 'phpdoc_types' => true,
        // 'phpdoc_var_without_name' => true,
        // 'no_blank_lines_after_phpdoc' => true,

        /**
         * Semicolon 分号
         */
        'multiline_whitespace_before_semicolons'     => true,
        'space_after_semicolon'                      => true,
        'no_empty_statement'                         => true,
        'space_after_semicolon'                      => true,
        'no_singleline_whitespace_before_semicolons' => true,

        /**
         * Whitespace
         */
        'no_spaces_around_offset' => true,

        'no_trailing_whitespace'      => true,
        'no_whitespace_in_blank_line' => true,
        'single_blank_line_at_eof'    => true,
        'statement_indentation'       => true,
        'no_extra_blank_lines'        => [
            'tokens' => [
                'curly_brace_block',
                'extra',
                // 'parenthesis_brace_block',
                // 'square_brace_block',
                'throw',
                'use',
            ],
        ],
        // 'heredoc_indentation' => ['indentation' => 'same_as_start'], // 需要 PHP7.3+

        /**
         * ------------------------------------------------------------
         * php项目文件头
         * ------------------------------------------------------------
         */
        'header_comment' => [
            'comment_type' => 'PHPDoc',
            'separate'     => 'bottom',
            'location'     => 'after_open',
            'header'       => <<<TEXT
轻量级的事件总线

@copyright (c) 2018-2024 上海宙品信息科技有限公司
TEXT
        ],
    ])
    ->setFinder($finder)
    ->setUsingCache(false)  // 禁用缓存
    ->setLineEnding("\n");
