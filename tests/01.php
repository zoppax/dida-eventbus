<?php
/**
 * Dida Framework  -- PHP轻量级快速开发框架
 * 版权所有 (c) 上海宙品信息科技有限公司
 *
 * Github: <https://github.com/zoppax/dida>
 * Gitee: <https://gitee.com/zoppax/dida>
 */

use Dida\EventBus;

require __DIR__ . "/_common.php";

// 定义一个APP.START事件
EventBus::addEvent("APP.START");

// 挂接一个普通函数
function a()
{
    echo "1\n";
}
EventBus::hook("APP.START", "a");

// 挂接一个普通函数，并传入函数参数
function b($p1, $p2)
{
    echo "$p1--$p2\n";
}
EventBus::hook("APP.START", "b", [3, 4]);

// 挂接一个匿名函数，且引用一个外部变量$b
$c = 999;
EventBus::hook("APP.START", function () use ($c) {
    echo "$c\n";
});

// 触发事件，执行事件挂接的所有回调函数
EventBus::trigger("APP.START");
