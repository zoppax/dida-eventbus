<?php
/**
 * 轻量级的事件总线
 *
 * @copyright (c) 2018-2024 上海宙品信息科技有限公司
 */

declare(strict_types=1);

namespace Dida;

/**
 * EventBus 事件总线
 */
class EventBus
{
    /**
     * 版本号
     */
    public const VERSION = '20240514';

    /**
     * 错误代码
     */
    public const ERROR_EVENT_NOT_FOUND = 1000;

    /**
     * hook模式开关
     * true： 自由模式。hook时，不检查event是否存在，直接hook。
     * false: 严格模式。hook时，先检查event是否存在。如果event不存在，则不批准本次hook。
     */
    public static $free_hook_mode = true;

    /*
     * 所有已经登记的事件
     * $events["event_name"] = true
     *
     * @var array
     */
    protected static $events = [];

    /*
     * 所有已经登记的事件回调函数
     * $hooks["event_name"][callback_id] = [$callback, $parameters]
     */
    protected static $hooks = [];

    /**
     * 如果值为true，则不往下执行后续事件回调函数
     */
    protected static $stop = false;

    /**
     * 当前事件上下文
     * 在EventBus::trigger() 前设置好，然后在就可以被各个回调函数使用。
     */
    public static $context = null;

    /**
     * 全局上下文
     * 这个很open，但是会吃内存，适可而止的用。
     */
    public static $global_context = null;

    /**
     * 检查一个事件是否已经存在。
     *
     * @param string $event 事件名称
     *
     * @return bool
     */
    public static function hasEvent(string $event): bool
    {
        return array_key_exists($event, self::$events);
    }

    /**
     * 注册一个event。
     *
     * @param string $event 事件名称
     *
     * @return void
     */
    public static function regEvent(string $event): void
    {
        self::$events[$event] = true;
    }

    /**
     * 注销一个用户事件，以及所有已挂接到这个事件上的回调函数。
     *
     * @param string $event 事件名称
     *
     * @return void
     */
    public static function unregEvent(string $event): void
    {
        unset(
            self::$events[$event],
            self::$hooks[$event]
        );
    }

    /**
     * regEvent的别名。
     */
    public static function addEvent(string $event): void
    {
        self::regEvent($event);
    }

    /**
     * unregEvent的别名。
     */
    public static function removeEvent(string $event): void
    {
        self::unregEvent($event);
    }

    /**
     * 绑定一个回调函数到指定事件上。
     *
     * @param string      $event       事件名称
     * @param callable    $callback    回调函数
     * @param array       $parameters  回调函数的参数。只允许传入索引数组，不可为关联数组。
     * @param string|null $callback_id 指定回调函数的id
     *
     * @return int|string|false 成功返回callback_id（int|string），失败返回false
     */
    public static function hook(string $event, callable $callback, array $parameters = [], ?string $callback_id = null)
    {
        // 如果事件不存在
        if (!self::hasEvent($event)) {
            // 根据$free_hook_mode开关，决定是否生成event
            // 如果free_hook_mode为true，则直接添加此event，然后注册callback。
            // 如果free_hook_mode为false，则返回false。
            if (self::$free_hook_mode) {
                self::addEvent($event);
            } else {
                return false;
            }
        }

        // 如果不指定id, 则callback放到队列尾部
        // 如果指定了id, 则用新的callback代替旧的callback
        if ($callback_id === null) {
            self::$hooks[$event][] = [$callback, $parameters];

            // 获取新增的callback_id
            $callback_id = key(end(self::$hooks[$event]));
        } else {
            // 用本callback替换旧的callback
            self::$hooks[$event][$callback_id] = [$callback, $parameters];
        }

        // 返回新增callback的id
        return $callback_id;
    }

    /**
     * 解除某个事件上挂接的某个或者全部回调函数。
     * 如果不指定id，则表示解除此事件上的所有回调函数。
     *
     * @param string      $event       事件名称
     * @param string|null $callback_id 回调函数的id
     *
     * @return void
     */
    public static function unhook(string $event, ?string $callback_id = null)
    {
        // 如果不指定id, 则删除这个event的所有callbacks
        // 如果指定了id, 则只删除event的指定callback
        if ($callback_id === null) {
            unset(self::$hooks[$event]);
        } else {
            unset(self::$hooks[$event][$callback_id]);
        }
    }

    /**
     * 触发一个事件，并执行挂接在这个事件上的所有回调函数。
     * 注：如果某个回调函数返回false，则不再执行后面的回调函数。
     *
     * @param string $event   事件名称
     * @param mixed  $context 事件上下文
     *                        可在回调事件中通过 EventBus::$context 访问到这个存入的变量
     *                        此处设为false时，不存入$context变量
     *
     * @return void
     */
    public static function trigger(string $event, $context = false): void
    {
        if (array_key_exists($event, self::$hooks)) {
            // 存入$context变量
            if ($context !== false) {
                self::$context = $context;
            }

            /*
             * 依次执行此事件上的所有回调函数.
             * 如果某个回调函数返回false，则不再执行后面的回调函数。
             */
            foreach (self::$hooks[$event] as $hook) {
                // 检查stop标志
                if (self::$stop) {
                    return;
                }

                // 拿到下个hook
                list($callback, $parameters) = $hook;

                // 执行回调函数
                call_user_func_array($callback, $parameters);
            }
        }
    }
}
